<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/23 18:01
 */

namespace Vars3cf;

use Swoole\Table as SwooleTable;

class Room
{
    /**
     * @var array
     */
    protected $config = [
        'room_rows'   => 4096,
        'room_size'   => 2048,
        'client_rows' => 8192,
        'client_size' => 2048,
    ];

    /**
     * @var SwooleTable
     */
    public $rooms;

    /**
     * @var SwooleTable
     */
    protected $fds;

    /**
     * TableRoom constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->initRoomsTable();
        $this->initFdsTable();
    }

    /**
     * Join sender to multiple rooms.
     *
     * @param fd, int $fd
     * @param string, array $rooms
     *
     * @return $this
     */
    public function join($fd, $rooms)
    {
        $rooms = is_string($rooms) || is_integer($rooms) ? func_get_args() : $rooms;

        $this->add($fd, $rooms);

        return $this;
    }

    /**
     * Make sender leave multiple rooms.
     *
     * @param int $fd
     * @param array $rooms
     *
     * @return $this
     */
    public function leave($fd, $rooms = [])
    {
        $rooms = is_string($rooms) || is_integer($rooms) ? func_get_args() : $rooms;

        $this->delete($fd, $rooms);

        return $this;
    }

    /**
     * Init rooms table
     */
    protected function initRoomsTable()
    {
        $this->rooms = new SwooleTable($this->config['room_rows']);
        $this->rooms->column('value', SwooleTable::TYPE_STRING, $this->config['room_size']);
        $this->rooms->create();
    }

    /**
     * Init descriptors table
     */
    protected function initFdsTable()
    {
        $this->fds = new SwooleTable($this->config['client_rows']);
        $this->fds->column('value', SwooleTable::TYPE_STRING, $this->config['client_size']);
        $this->fds->create();
    }

    /**
     * Add multiple socket fds to a room.
     *
     * @param int fd
     * @param array|string rooms
     */
    public function add(int $fd, $roomNames)
    {
        $rooms     = $this->getRooms($fd);
        $roomNames = is_array($roomNames) ? $roomNames : [$roomNames];

        foreach ($roomNames as $room) {
            $fds = $this->getClients($room);

            if (in_array($fd, $fds)) {
                continue;
            }

            $fds[]   = $fd;
            $rooms[] = $room;

            $this->setClients($room, $fds);
        }

        $this->setRooms($fd, $rooms);
    }

    /**
     * Delete multiple socket fds from a room.
     *
     * @param int fd
     * @param array|string rooms
     */
    public function delete(int $fd, $roomNames = [])
    {
        $allRooms  = $this->getRooms($fd);
        $roomNames = is_array($roomNames) ? $roomNames : [$roomNames];
        $rooms     = count($roomNames) ? $roomNames : $allRooms;

        $removeRooms = [];
        foreach ($rooms as $room) {
            $fds = $this->getClients($room);

            if (!in_array($fd, $fds)) {
                continue;
            }

            $this->setClients($room, array_values(array_diff($fds, [$fd])));
            $removeRooms[] = $room;
        }

        $this->delValue($fd, 'fds');
    }

    /**
     * Get all sockets by a room key.
     *
     * @param string room
     *
     * @return array
     */
    public function getClients(string $room)
    {
        return $this->getValue($room, 'rooms') ?? [];
    }

    /**
     * Get all rooms by a fd.
     *
     * @param int fd
     *
     * @return array
     */
    public function getRooms(int $fd)
    {
        return $this->getValue($fd, 'fds') ?? [];
    }

    /**
     * @param string $room
     * @param array  $fds
     *
     * @return $this
     */
    protected function setClients(string $room, array $fds)
    {
        return $this->setValue($room, $fds, 'rooms');
    }

    /**
     * @param int   $fd
     * @param array $rooms
     *
     * @return $this
     */
    protected function setRooms(int $fd, array $rooms)
    {
        return $this->setValue($fd, $rooms, 'fds');
    }

    /**
     * Set value to table
     *
     * @param        $key
     * @param array  $value
     * @param string $table
     *
     * @return $this
     */
    public function setValue($key, array $value, string $table)
    {
        //$this->checkTable($table);

        $this->$table->set($key, ['value' => json_encode($value)]);

        return $this;
    }

    /**
     * Get value from table
     *
     * @param string $key
     * @param string $table
     *
     * @return array|mixed
     */
    public function getValue(string $key, string $table)
    {
        //$this->checkTable($table);

        $value = $this->$table->get($key);

        return $value ? json_decode($value['value'], true) : [];
    }

    /**
     * Del value to table
     *
     * @param        $key
     * @param array  $value
     * @param string $table
     *
     * @return $this
     */
    public function delValue(string $key, string $table)
    {
        //$this->checkTable($table);

        $this->$table->del($key);

        return $this;

    }

    /**
     * Check table for exists
     *
     * @param string $table
     */
    protected function checkTable(string $table)
    {
        if (!method_exists($this, $table) || !$this->$table) {
            throw new \Exception("Invalid table name: `{$table}`.");
        }
    }
}