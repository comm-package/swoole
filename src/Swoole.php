<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/2 15:39
 */

namespace Vars3cf;

class Swoole
{
    // http服务
    protected $http_server;
    public $http_request;

    // websocket服务
    protected $websocket_server;
    public $websocket_open;
    public $websocket_message;
    public $websocket_close;
    public $websocket_request;
    public $websocket_task;
    public $websocket_finish;

    /*
     * 构造方法
     */
    public function __construct($config)
    {
        // 多例服务
        $server_names = array_column($config['servers'], 'name', 'http');
        if (in_array('http', $server_names) && in_array('ws', $server_names)) {
            $httpConfig = $config['servers'][array_keys($server_names, 'http')[0]];
            $wsConfig = $config['servers'][array_keys($server_names, 'ws')[0]];
            $this->initWebSocket($wsConfig);

            // 监听http端口
            $http = $this->websocket_server->listen($httpConfig['host'], $httpConfig['port'], SWOOLE_SOCK_TCP);
            $httpConfig['settings']['open_http_protocol'] = true;
            $http->set($httpConfig['settings']);
            $this->http_request = $httpConfig['callbacks']['onRequest'];
            $http->on('Request', [$this, 'onRequest']);

            // 启动服务
            $this->onStartWebSocket();
        }

        // 单例服务
        foreach ($config['servers'] as $row) {
            if ($row['name'] == 'http') {
                $this->initHttp($row);
                $this->onStartHttp();
            }
            if ($row['name'] == 'ws') {
                $this->initWebSocket($row);
                $this->onStartWebSocket();
            }
        }
    }

    // 初始化websocket
    public function initWebSocket($config)
    {
        $this->websocket_server = new \Swoole\WebSocket\Server($config['host'], $config['port'], SWOOLE_PROCESS);
        $config['settings']['open_http_protocol'] = false;
        $this->websocket_server->set($config['settings']);
        $this->websocket_open = $config['callbacks']['onOpen'];
        $this->websocket_message = $config['callbacks']['onMessage'];
        $this->websocket_close = $config['callbacks']['onClose'];
        $this->websocket_request = $config['callbacks']['onRequest'];
        $this->websocket_task = $config['callbacks']['onTask'];
        $this->websocket_finish = $config['callbacks']['onFinish'];

        $this->websocket_server->on('open', [$this, 'onOpen']);
        $this->websocket_server->on('message', [$this, 'onMessage']);
        $this->websocket_server->on('request', [$this, 'onWsRequest']);
        $this->websocket_server->on('close', [$this, 'onClose']);
        if (isset($config['settings']['task_enable_coroutine']) && $config['settings']['task_enable_coroutine']) {
            $this->websocket_server->on('task', [$this, 'onTaskCoroutine']);
        } else {
            $this->websocket_server->on('task', [$this, 'onTask']);
        }
        $this->websocket_server->on('finish', [$this, 'onFinish']);

        // 创建 table
        if (isset($config['room']['table']) && $config['room']['table']) {
            $this->websocket_server->rooms = new Room($config['room']['table']);
        }
    }

    // 初始化http
    public function initHttp($config)
    {
        $this->http_server = new \Swoole\Http\Server($config['host'], $config['port'], SWOOLE_PROCESS);
        $this->http_server->set($config['settings']);
        $this->http_request = $config['callbacks']['onRequest'];

        $this->http_server->on('Request', [$this, 'onRequest']);
    }

    // 启动websocket服务
    public function onStartWebSocket()
    {
        echo "The WebSocket Service Started Successfully! Start Time : " . date('Y-m-d H:i:s') . "\n";
        $this->websocket_server->start();
    }

    // 启动http服务
    public function onStartHttp()
    {
        echo "The Http Service Started Successfully! Start Time : " . date('Y-m-d H:i:s') . "\n";
        $this->http_server->start();
    }

    // 监听http请求事件
    public function onRequest($request, $response)
    {
        if ($this->http_request) {
            new $this->http_request($this->websocket_server, $request, $response);
        }
    }

    // 打开链接
    public function onOpen($server, $request)
    {
        if ($this->websocket_open) {
            new $this->websocket_open($server, $request);
        }
    }

    // 接受消息
    public function onMessage($server, $frame)
    {
        if ($this->websocket_message) {
            new $this->websocket_message($server, $frame);
        }
    }

    // 关闭链接
    public function onClose($server, $fd)
    {
        if ($this->websocket_close) {
            new $this->websocket_close($server, $fd);
        }
    }

    // 监听http请求事件
    public function onWsRequest($request, $response)
    {
        if ($this->websocket_request) {
            new $this->websocket_request($request, $response);
        }
    }

    // 处理异步任务
    public function onTask($server, $task_id, $reactor_id, $data)
    {
        if ($this->websocket_task) {
            new $this->websocket_task($server, $task_id, $reactor_id, $data);
        }
    }

    // 处理异步任务(协程)
    public function onTaskCoroutine($server, $task)
    {
        if ($this->websocket_task) {
            new $this->websocket_task($server, $task);
        }
    }

    // 处理异步任务的结果
    public function onFinish($server, $task_id, $data)
    {
        if ($this->websocket_finish) {
            new $this->websocket_finish($server, $task_id, $data);
        }
    }
}