<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/2 15:40
 */

require_once __DIR__ . '/vendor/autoload.php';

use Vars3cf\Swoole;

// 读取配置文件
$config = require_once 'swoole.php';

// 启动服务
new Swoole($config);