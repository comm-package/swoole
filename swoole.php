<?php

// +----------------------------------------------------------------------
// | Swoole 配置示例
// +----------------------------------------------------------------------

return [
    'mode' => SWOOLE_PROCESS,
    'servers' => [
        // http服务配置示例
        [
            'name' => 'http',
            'host' => '0.0.0.0',
            'port' => 9501,
            'sock_type' => SWOOLE_SOCK_TCP,
            'callbacks' => [
                'onRequest' => \app\http\onRequest::class,
            ],
            'settings' => [
                'daemonize' => 0, //守护进程
                'worker_num' => 2, //设置启动的 Worker 进程数
                'socket_buffer_size' => 128 * 1024 *1024, //配置客户端连接的缓存区长度
                'buffer_output_size' => 10 * 1024 * 1024, //配置发送输出缓存区内存尺寸
            ],
        ],
        // websocket服务配置示例
        [
            'name' => 'ws',
            'host' => '0.0.0.0',
            'port' => 9502,
            'sock_type' => SWOOLE_SOCK_TCP,
            'callbacks' => [
                'onOpen' => \app\websocket\onOpen::class,
                'onMessage' => \app\websocket\onMessage::class,
                'onClose' => \app\websocket\onClose::class,
                'onRequest' => \app\websocket\onRequest::class,
                'onTask' => \app\websocket\onTask::class,
                'onFinish' => \app\websocket\onFinish::class,
            ],
            'settings' => [
                'daemonize' => 0, //守护进程
                'worker_num' => 2, //设置启动的 Worker 进程数
                'task_worker_num' => 4, //配置 Task 进程的数量
                'task_enable_coroutine' => false, //任务是否使用协程
                'max_request' => 100000, //最大请求数
                'socket_buffer_size' => 128 * 1024 *1024, //配置客户端连接的缓存区长度
                'buffer_output_size' => 10 * 1024 * 1024, //配置发送输出缓存区内存尺寸
            ],
            'room' => [
                'table' => [
                    'room_rows'   => 4096,
                    'room_size'   => 2048,
                    'client_rows' => 8192,
                    'client_size' => 2048,
                ],
            ],
        ],
    ],
];