
### 介绍
    Swoole 是一个使用 C++ 语言编写的基于异步事件驱动和协程的并行网络
    通信引擎，为 PHP 提供协程、高性能网络编程支持。提供了多种通信协议
    的网络服务器和客户端模块，可以方便快速的实现 TCP/UDP服务、高性能
    Web、WebSocket服务、物联网、实时通讯、游戏、微服务等，
    使 PHP 不再局限于传统的 Web 领域
    
    该swoole包引用后，配置好文件可直接在项目中使用swoole服务

### 环境要求
    php-7.3 或更高版本
    gcc-4.8 或更高版本
    swoole4.8扩展 或更高版本
    
### 软件架构
    - src 核心应用
	    - Swoole 服务
    - swoole.php 配置文件示例
    - demo.php 启动服务示例
    
### 安装教程
    - composer require liangmg/swoole (引用包)
    - cp demo.php app/demo.php (复制启动文件到项目中)
    - cp swoole.php app/demo.php (复制配置文件到项目中)
    - php app/demo.php (启动服务) (注：启动服务前，最好先配置好回调事件)
    - websocket访问 ws://127.0.0.1:9502/
    - http访问 http://127.0.0.1:9502
    
### 回调事件配置
    - 如配置websocket的onOpen事件，你的控制器可以这样写
        <?php
        namespace app\websocket;
        class onOpen
        {
            public function __construct($server, $request)
            {
                echo "server: handshake success with fd \n";
            }
        }
        
    - onMessage，onClose等其他事件，参数可参考swoole文档配置即可

### 房间事件
    - 加入房间
        $server->rooms->join($fd, 'room1');
    - 离开房间
        $server->rooms->leave($fd, 'room1');
    - 获取房间所有用户
        $rm = $server->rooms->getClients('room1');

    


